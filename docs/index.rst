.. Django HTMX documentation master file, created by
   sphinx-quickstart on Tue Jun 30 14:36:43 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Django HTMX's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
